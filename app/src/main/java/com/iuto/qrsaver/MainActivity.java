package com.iuto.qrsaver;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import com.iuto.qrsaver.database.RoomDatabase;
import com.iuto.qrsaver.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;
    private static NavController controller;
    public static com.iuto.qrsaver.database.RoomDatabase roomDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //deleteDatabase("qr_database");
        roomDatabase = RoomDatabase.getDatabase(MainActivity.this);
        //new InstanceBD(); // cree un jeu de données pour la BD

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());


        BottomNavigationView navView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_home, R.id.navigation_scan, R.id.navigation_settings)
                .build();
        controller = Navigation.findNavController(this, R.id.nav_host_fragment_activity_main);
        NavigationUI.setupActionBarWithNavController(this, controller, appBarConfiguration);
        NavigationUI.setupWithNavController(binding.navView, controller);
    }

    public static void updateNavColor(View view, int newColor) {
        Context c = view.getContext();
        /*MenuItem home = view.findViewById(R.id.navigation_home);
        home.getIcon().setColorFilter(ContextCompat.getColor(c, newColor), PorterDuff.Mode.SRC_IN);
        MenuItem scan = view.findViewById(R.id.navigation_scan);
        scan.getIcon().setColorFilter(ContextCompat.getColor(c, newColor), PorterDuff.Mode.SRC_IN);
        MenuItem settings = view.findViewById(R.id.navigation_settings);
        settings.getIcon().setColorFilter(ContextCompat.getColor(c, newColor), PorterDuff.Mode.SRC_IN);*/
        BottomNavigationView navView = view.findViewById(R.id.nav_view);
        view.setBackgroundColor(newColor);
    }

    public static void navigateHome() {
        controller.navigate(R.id.navigation_home);
    }
}