package com.iuto.qrsaver.ui.home;


import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.iuto.qrsaver.MainActivity;
import com.iuto.qrsaver.R;
import com.iuto.qrsaver.adapter.QrAdapter;
import com.iuto.qrsaver.database.QrModel;
import com.iuto.qrsaver.database.RoomDAO;

public class Popup_supprimer extends Dialog {
    private Context context;
    private View.OnClickListener listener;
    private QrModel currentQr;


    public Popup_supprimer(Context contexte, View.OnClickListener onClickListener, QrModel current) {
        super(contexte);
        this.currentQr = current;
        this.context = contexte;
        this.listener = onClickListener;

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.popup_supprimer);
        Button button = findViewById(R.id.popup_supprimer_valider);

        button.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new Thread(
                                ()->{
                                    RoomDAO roomDAO = MainActivity.roomDatabase.roomDAO();
                                    roomDAO.deleteQr(currentQr.getId());
                                }
                        ).start();

                        dismiss();

                    }}
        );



    }
}