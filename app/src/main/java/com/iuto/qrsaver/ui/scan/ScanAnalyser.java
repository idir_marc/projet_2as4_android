package com.iuto.qrsaver.ui.scan;

import android.annotation.SuppressLint;
import android.media.Image;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.camera.core.ImageAnalysis;
import androidx.camera.core.ImageProxy;
import androidx.fragment.app.Fragment;
import com.google.mlkit.vision.barcode.common.Barcode;
import com.google.mlkit.vision.barcode.BarcodeScanner;
import com.google.mlkit.vision.barcode.BarcodeScannerOptions;
import com.google.mlkit.vision.barcode.BarcodeScanning;
import com.google.mlkit.vision.common.InputImage;
import com.iuto.qrsaver.MainActivity;
import com.iuto.qrsaver.database.QrModel;
import com.iuto.qrsaver.database.RoomDatabase;

public class ScanAnalyser implements ImageAnalysis.Analyzer{
    private final Fragment frag;
    BarcodeScannerOptions options = new BarcodeScannerOptions.Builder()
            .setBarcodeFormats(Barcode.FORMAT_QR_CODE, Barcode.FORMAT_AZTEC)
            .build();

    public ScanAnalyser(Fragment fragment) {
        frag = fragment;
    }

    @Override
    public void analyze(@NonNull ImageProxy image) {
        @SuppressLint("UnsafeOptInUsageError") Image img = image.getImage();
        if (img != null) {
            InputImage inputImage = InputImage.fromMediaImage(img, image.getImageInfo().getRotationDegrees());
            BarcodeScanner scanner = BarcodeScanning.getClient(options);

            scanner.process(inputImage)
                    .addOnSuccessListener( barcodes -> {
                        for (Barcode code : barcodes) {
                            // handle
                            //Toast.makeText(frag.getContext(), code.getDisplayValue(), Toast.LENGTH_SHORT).show();
                            new Saver(code.getDisplayValue()).start();
                            MainActivity.navigateHome();
                        }
                    })
                    .addOnFailureListener( (e) -> {});
        }

        image.close();
    }

    private class Saver extends Thread {
        String save;
        public Saver(String s) {
            save = s;
        }

        @Override
        public void run() {
            Log.d("QrSaver", "Qr Code saved");
            QrModel qr = new QrModel(save);
            RoomDatabase.getDatabase(frag.getContext()).roomDAO().insert(qr);
        }
    }
}
