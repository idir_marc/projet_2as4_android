package com.iuto.qrsaver.ui;

import android.os.Bundle;
import androidx.preference.PreferenceFragmentCompat;
import com.iuto.qrsaver.R;

public class SettingsFragment extends PreferenceFragmentCompat {

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.root_preferences, rootKey);
    }
}