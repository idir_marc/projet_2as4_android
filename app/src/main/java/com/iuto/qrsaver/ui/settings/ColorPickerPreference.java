package com.iuto.qrsaver.ui.settings;

import android.content.Context;
import android.os.Bundle;
import android.util.AttributeSet;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.preference.DialogPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragment;
import androidx.preference.PreferenceViewHolder;
import com.iuto.qrsaver.R;
import org.jetbrains.annotations.NotNull;

public class ColorPickerPreference extends PreferenceFragment {
    private int mColor;
    private ColorPickerDialog dialog;

    public int getValue() {
        return mColor;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onCreatePreferences(@Nullable Bundle savedInstanceState, String rootKey) {
        //
    }
}
