package com.iuto.qrsaver.ui.scan;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.camera.core.CameraSelector;
import androidx.camera.core.ImageAnalysis;
import androidx.camera.core.Preview;
import androidx.camera.lifecycle.ProcessCameraProvider;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.common.util.concurrent.ListenableFuture;
import com.iuto.qrsaver.R;
import com.iuto.qrsaver.databinding.FragmentScanBinding;

import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ScanFragment extends Fragment {
    ExecutorService cameraExecutor;
    private FragmentScanBinding binding;

    public ScanFragment() {
        // Required empty constructor
    }

    @Override
    public void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cameraExecutor = Executors.newSingleThreadExecutor();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentScanBinding.inflate(inflater, container, false);
        if (checkCameraPermission()) {
            startCamera();
        }
        return binding.getRoot();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        cameraExecutor.shutdown();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    private boolean checkCameraPermission() {
        try {
            Objects.requireNonNull(getActivity());
            String[] permissions = new String[]{Manifest.permission.CAMERA};
            ActivityCompat.requestPermissions(this.getActivity(), permissions, 0);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
        return checkCameraPermissionGranted();
    }

    private boolean checkCameraPermissionGranted() {
        Objects.requireNonNull(getContext());
        if (ContextCompat.checkSelfPermission(this.getContext(),
                Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            Resources res = getResources();
            @SuppressLint("StringFormatMatches")
            String message = String.format(res.getString(R.string.permission_required_message),
                    res.getString(R.string.camera_name),
                    res.getString(R.string.qr_goal_name));
            AlertDialog dialog = new MaterialAlertDialogBuilder(this.getContext())
                    .setTitle(R.string.permission_required)
                    .setMessage(message)
                    .setPositiveButton("OK", (a,b) -> checkCameraPermission())
                    .setNeutralButton(R.string.settings,
                            (a,b) -> startActivity(new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                    Uri.parse("package:"+getContext().getPackageName()))))
                    .setCancelable(false)
                    .create();
            //dialog.setCanceledOnTouchOutside(false);
            dialog.show();
            return false;
        }
    }

    private void startCamera() {
        assert getContext() != null;
        ListenableFuture<ProcessCameraProvider> cameraFuture = ProcessCameraProvider.getInstance(getContext());
        cameraFuture.addListener(() -> {
            try {
                ProcessCameraProvider cameraProvider = cameraFuture.get();

                Preview preview =  new Preview.Builder().build();
                preview.setSurfaceProvider(binding.qrPreview.getSurfaceProvider());

                ImageAnalysis imageAnalysis = new ImageAnalysis.Builder()
                        .setBackpressureStrategy(ImageAnalysis.STRATEGY_KEEP_ONLY_LATEST).build();
                imageAnalysis.setAnalyzer(cameraExecutor, new ScanAnalyser(ScanFragment.this));

                CameraSelector selector = CameraSelector.DEFAULT_BACK_CAMERA;
                cameraProvider.unbindAll();
                cameraProvider.bindToLifecycle(ScanFragment.this, selector, preview, imageAnalysis);

                System.out.println("oups!");
            } catch (ExecutionException | InterruptedException e) {
                e.printStackTrace();
            }
        }, ContextCompat.getMainExecutor(getContext()));
    }
}