package com.iuto.qrsaver.ui.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;
import com.iuto.qrsaver.MainActivity;
import com.iuto.qrsaver.R;
import com.iuto.qrsaver.adapter.QrAdapter;
import com.iuto.qrsaver.database.QrModel;
import com.iuto.qrsaver.database.RoomDAO;
import com.iuto.qrsaver.databinding.FragmentHomeBinding;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;
    private FragmentHomeBinding binding;
    private RoomDAO roomDAO;
    private List<QrModel> listeQr;


    @Override
    public void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel = new ViewModelProvider(this).get(HomeViewModel.class);
        binding = FragmentHomeBinding.inflate(inflater, container, false);
        View root = binding.getRoot();


        Button button_trier = root.getRootView().findViewById(R.id.button_trier);
        button_trier.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (currentSelectedItem != spinner_tri.getSelectedItem().toString()){
                        currentSelectedItem = spinner_tri.getSelectedItem().toString();
                        attendre = true;
                        new Thread(
                                ()->{


                                    if (currentSelectedItem.equals("Plus récent")){
                                        qrModels = roomDAO.getAllQr();
                                        qrAdapter.setListeQr(qrModels);

                                    } else if (currentSelectedItem.equals("Par ordre alphabétique")){
                                        qrModels = roomDAO.getAllQrName();
                                        qrAdapter.setListeQr(qrModels);

                                    } else if (currentSelectedItem.equals("Plus ancien")){
                                        qrModels = roomDAO.getAllQrAncien();
                                        qrAdapter.setListeQr(qrModels);
                                    } else {
                                        //qqc ici
                                    }
                                    attendre = false;

                                }
                        ).start();
                            while (attendre){} //on att que le thread finisse
                            qrAdapter.notifyDataSetChanged();
                        }


                    }}
        );



        qrAdapter = new QrAdapter(listeQr, R.layout.itemqr, getActivity());
        roomDAO = MainActivity.roomDatabase.roomDAO();
        roomDAO.LDgetAllQrRecent().observe(this, new Observer<List<QrModel>>() {
            @Override
            public void onChanged(List<QrModel> qrModels) {
                qrAdapter.setListeQr(qrModels);
                qrAdapter.notifyDataSetChanged();
            }
        });


        new Thread(
                ()->{
                    spinner_tri = root.getRootView().findViewById(R.id.spinner_tri_enregistrement);
                    List<String> T = new ArrayList<>();
                    T.add("Plus récent");
                    T.add("Par ordre alphabétique");
                    T.add("Plus ancien");
                    T.add("Par tag");
                    ArrayAdapter<String> da = new ArrayAdapter<String>(getContext(), R.layout.support_simple_spinner_dropdown_item, T);
                    spinner_tri.setAdapter(da);


                    listeQr = roomDAO.getAllQr();
                    RecyclerView verticalRecyclerViewQr = root.getRootView().findViewById(R.id.vertical_recycler_view_qrcode);
                    verticalRecyclerViewQr.setAdapter(qrAdapter);
                }
        ).start();

        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}