package com.iuto.qrsaver.ui.settings;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.TextureView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import com.google.android.material.slider.Slider;
import com.google.android.material.textfield.TextInputLayout;
import com.iuto.qrsaver.MainActivity;
import com.iuto.qrsaver.databinding.ColorPickerDialogBinding;

public class ColorPickerDialog extends DialogFragment {
    Slider red, green, blue;
    TextInputLayout hex;
    TextureView color;
    Button ok, cancel;
    ColorPickerDialogBinding binding;

    boolean err;
    int newColor;

    public ColorPickerDialog() {
        this(0);
    }

    public ColorPickerDialog(int value) {
        super();
        newColor = value;
    }

    public ColorPickerDialog(int value, @LayoutRes int contentLayoutId) {
        super(contentLayoutId);
        newColor = value;
    }

    @SuppressLint("ClickableViewAccessibility")
    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        LayoutInflater inflater = requireActivity().getLayoutInflater();
        binding = ColorPickerDialogBinding.inflate(inflater);
        red     = binding.colorpickerSliderRed;
        green   = binding.colorpickerSliderGreen;
        blue    = binding.colorpickerSliderBlue;
        hex     = binding.colorpickerHex;
        color   = binding.colorpickerColor;
        ok      = binding.colorpickerButtonOk;
        cancel  = binding.colorpickerButtonCancel;
        err = false;
        // styling
        ok.setBackgroundColor(Color.BLUE);
        // listeners and such
        super.setCancelable(true);
        cancel.setOnClickListener(view -> super.dismiss());
        ok.setOnClickListener(this::onOk);
        hex.addOnEditTextAttachedListener(this::onTextEdit);
        //red.addOnSliderTouchListener(new SliderListener());
        red.setOnTouchListener(new SliderTouchListener(red));
        green.setOnTouchListener(new SliderTouchListener(green));
        blue.setOnTouchListener(new SliderTouchListener(blue));
        return super.onCreateDialog(savedInstanceState);
    }

    int setValue(int val) {
        int i = newColor;
        newColor = val;
        updateColor();
        return i;
    }

    private void updateColor() {
        Bitmap bitmap = Bitmap.createBitmap(new int[]{newColor}, hex.getHeight(), hex.getWidth(), Bitmap.Config.RGB_565);
        hex.draw(new Canvas(bitmap));
    }

    private void onTextEdit(TextInputLayout layout) {
        String text = layout.getEditText().getText().toString();
        if (text.length() != 6 && text.length() != 3 |! isHex(text)) {
            layout.setError("Invalid error hex");
            ok.setBackgroundColor(Color.GRAY);
            err = true;
            return;
        }
        ok.setBackgroundColor(Color.BLUE);
        err = false;
        setValue(Color.parseColor(text));
    }

    private static boolean isHex(String s) {
        for(char c : s.toLowerCase().toCharArray()) {
            if (c == 32) continue;
            if (c < 48 || (c > 57 &&  c < 97) || c > 102) return false;
        }
        return true;
    }

    private void onOk(View view) {
        if (err) return;
        getActivity().getWindow().setNavigationBarColor(newColor);
        MainActivity.updateNavColor(getView(),newColor);
    }

    private class SliderListener implements Slider.OnSliderTouchListener {

        @Override
        public void onStartTrackingTouch(@NonNull Slider slider) {
        }

        @Override
        public void onStopTrackingTouch(@NonNull Slider slider) {
        }
    }

    private class SliderTouchListener implements View.OnTouchListener {
        Slider target;

        public SliderTouchListener(Slider slider) {
            target = slider;
        }

        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            //target.onTouchEvent(motionEvent);
            EditText text = ColorPickerDialog.this.hex.getEditText();
            String s = String.format("%s%s", Integer.toHexString((int) target.getValue()),
                    text.getText().toString().substring(2,6));
            text.setText(s);
            setValue(Color.parseColor(s));
            return false;
        }
    }
}
