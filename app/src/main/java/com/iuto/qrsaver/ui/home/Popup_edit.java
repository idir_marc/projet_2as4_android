package com.iuto.qrsaver.ui.home;


import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.*;
import androidx.recyclerview.widget.RecyclerView;
import com.iuto.qrsaver.MainActivity;
import com.iuto.qrsaver.R;
import com.iuto.qrsaver.adapter.QrAdapter;
import com.iuto.qrsaver.database.QrModel;
import com.iuto.qrsaver.database.QrThModel;
import com.iuto.qrsaver.database.RoomDAO;
import com.iuto.qrsaver.database.ThemeModel;

import java.util.ArrayList;
import java.util.List;

public class Popup_edit extends Dialog {
    private Context context;
    private View.OnClickListener listener;
    private QrModel currentQr;
    private TextView tv;
    private Spinner spinner_theme1;
    private Spinner spinner_theme2;
    private List<Integer> idThemes;

    public Popup_edit(Context contexte, View.OnClickListener onClickListener, QrModel current, TextView tv_nom) {
        super(contexte);
        this.currentQr = current;
        this.context = contexte;
        this.listener = onClickListener;
        this.tv = tv_nom;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.popup_edit);
        EditText ET_name = findViewById(R.id.popup_edit_name);
        Button button = findViewById(R.id.popup_edit_valider);


        //spinner



        new Thread(
                ()->{
                     spinner_theme1 = findViewById(R.id.spinner_theme1);
                     spinner_theme2 = findViewById(R.id.spinner_theme2);
                    RoomDAO roomDAO = MainActivity.roomDatabase.roomDAO();
                    idThemes = roomDAO.getIdThemeAvecIdQR(currentQr.getId());

                    String name1 = null;
                    String name2 = null;

                    if (idThemes.size() > 0){
                         name1 = roomDAO.getThemeName(idThemes.get(0));
                    }

                    if (idThemes.size() > 1){
                         name2 = roomDAO.getThemeName(idThemes.get(1));
                    }


                    List<String> T = roomDAO.getAllThemeName();
                    T.add(0, "Aucun");
                    List<String> T2 = new ArrayList<>(T);


                    if (name1 != null){
                        T.remove(name1);
                        T.add(0, name1);
                    }
                    ArrayAdapter<String> da = new ArrayAdapter<String>(getContext(), R.layout.support_simple_spinner_dropdown_item, T);


                    spinner_theme1.setAdapter(da);
                    if (name2 != null){
                        T2.remove(name2);
                        T2.add(0, name2);
                    }
                    ArrayAdapter<String> da2 = new ArrayAdapter<String>(getContext(), R.layout.support_simple_spinner_dropdown_item, T2);
                    spinner_theme2.setAdapter(da2);
                }
        ).start();








        button.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new Thread(
                                ()->{
                                    RoomDAO roomDAO = MainActivity.roomDatabase.roomDAO();
                                    roomDAO.deleteAllThQr(currentQr.getId());
                                    roomDAO.updateNameQr(currentQr.getId(), ET_name.getText().toString());
                                    List<QrThModel> x = roomDAO.getThQr(currentQr.getId());
                                    for (int j = 0; j < x.size(); j++){
                                        if (j == 0){
                                            x.get(j).setId_theme(0);
                                        } else {
                                            x.get(j).setId_theme(1);
                                        }

                                    }
                                    String s1 = spinner_theme1.getSelectedItem().toString();
                                    String s2 = spinner_theme2.getSelectedItem().toString();

                                    if (!s1.equals("aucun")){
                                        int t = roomDAO.getThemeId(s1);
                                        roomDAO.insert(new QrThModel(t,currentQr.getId()));
                                    }

                                    if (!s2.equals("aucun") && s1.equals(s2)){
                                        int t = roomDAO.getThemeId(s2);
                                        roomDAO.insert(new QrThModel(t,currentQr.getId()));
                                    }


                                }
                        ).start();

                        dismiss();

                    }}
        );
    }
}