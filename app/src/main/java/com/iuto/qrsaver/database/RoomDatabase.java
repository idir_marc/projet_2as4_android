package com.iuto.qrsaver.database;

import android.content.Context;
import androidx.room.Database;
import androidx.room.Room;

@Database(entities = {QrModel.class, TagModel.class, QrTagModel.class}, version = 1)
public abstract class RoomDatabase extends androidx.room.RoomDatabase {
    private static RoomDatabase INSTANCE;

    public abstract RoomDAO roomDAO();

    public static RoomDatabase getDatabase(final Context context){
        if (INSTANCE == null){
            synchronized (RoomDatabase.class){
                if (INSTANCE == null){
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            RoomDatabase.class, "qr_database").build();
                }
            }
        }
        return INSTANCE;
    }
}