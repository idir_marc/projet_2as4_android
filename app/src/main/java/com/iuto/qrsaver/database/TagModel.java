package com.iuto.qrsaver.database;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "ThemeModel")
public class TagModel {

    @PrimaryKey
    private int id;

    public String name;

    @NonNull
    public String description;
    public String couleur;

    public TagModel(int id, String name, String couleur, String description){
        this.couleur =couleur;
        this.description = description;
        this.name = name;
        this.id = id;
    }

    public TagModel(){
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCouleur() {
        return couleur;
    }

    public void setCouleur(String couleur) {
        this.couleur = couleur;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
