package com.iuto.qrsaver.database;

import com.iuto.qrsaver.MainActivity;

public class InstanceBD {
    private RoomDAO roomDAO;
    private RoomDatabase roomDatabase;

    public InstanceBD(){
        roomDatabase = MainActivity.roomDatabase;
        creaInstance();
    }

    public void creaInstance(){
        roomDAO = roomDatabase.roomDAO();

        new Thread(
                ()->{

                    QrModel q1 = new QrModel("http://avion.com");
                    QrModel q2 = new QrModel("http://chaussure.com");
                    QrModel q3 = new QrModel("http://ballon.com");
                    QrModel q4 = new QrModel("http://aviondechasse.com");
                    QrModel q5 = new QrModel("http://chercheunstage.com");
                    roomDAO.insert(q1);
                    roomDAO.insert(q2);
                    roomDAO.insert(q3);
                    roomDAO.insert(q4);
                    roomDAO.insert(q5);


                    TagModel t1 = new TagModel(1,null,"orange","avions de chasse");
                    TagModel t2 = new TagModel(2,"professionel","rouge","description professionel");
                    TagModel t3 = new TagModel(3,"loisirs","bleu","description loisirs");
                    roomDAO.insert(t1);
                    roomDAO.insert(t2);
                    roomDAO.insert(t3);


                    QrThModel qt1 = new QrThModel(1,1);
                    QrThModel qt2 = new QrThModel(2,1);
                    QrThModel qt3 = new QrThModel(3,1);
                    QrThModel qt4 = new QrThModel(2,2);
                    QrThModel qt5 = new QrThModel(3,4);
                    roomDAO.insert(qt1);
                    roomDAO.insert(qt2);
                    roomDAO.insert(qt3);
                    roomDAO.insert(qt4);
                    roomDAO.insert(qt5);

                }
        ).start();
    }


}
