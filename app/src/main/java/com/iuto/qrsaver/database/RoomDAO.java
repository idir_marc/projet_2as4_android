package com.iuto.qrsaver.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface RoomDAO {


    //insert
    @Insert
    void insert(QrModel qrModel);

    @Insert
    void insert(TagModel tagModel);

    @Insert
    void insert(QrTagModel qrTagModel);

    //get info
    //spinner
    @Query("select * from QrModel")
    LiveData<List<QrModel>> LDgetAllQrRecent();

    @Query("select * from QrModel order by date DESC")
    List<QrModel> getAllQrAncien();

    @Query("select * from QrModel order by date ASC")
    List<QrModel> getAllQrRecent();

    @Query("select * from QrModel order by (case when name is null then 1 else 0 end), name")
    List<QrModel> getAllQrName();



    @Query("select * from QrModel")
    List<QrModel> getAllQr();

    @Query("select name from ThemeModel")
    List<String> getAllThemeName();

    @Query("select couleur from ThemeModel where id = :id")
    String getEnregistrementCouleur(int id);

    @Query("select id_theme from QrThModel where id_Qr = :idQr")
    List<Integer> getIdThemeAvecIdQR(int idQr);


    @Query("select name from ThemeModel where id = :id")
    String getThemeName(int id);

    @Query("select id from ThemeModel where name= :name")
    int getThemeId(String name);

    @Query("select * from QrThModel where id_Qr= :id")
    List<QrThModel> getThQr(int id);


    //delete
    @Query("DELETE from QrModel where id= :id")
    void deleteQr(int id);

    @Query("DELETE from QrThModel where id_theme= :idTh and id_Qr = :idQr")
    void deleteThQr(int idTh, int idQr);

    @Query("DELETE from QrThModel where  id_Qr = :idQr")
    void deleteAllThQr(int idQr);







    //set info
    @Query("UPDATE QrModel SET name = :nom WHERE id = :id")
    void updateNameQr(Integer id, String nom);

    @Query("UPDATE QrThModel SET id_theme = :idTh WHERE id_Qr = :idQr and id_theme != :id_autreTheme")
    void updateThemeQr(Integer idQr, int idTh, int id_autreTheme);




}
