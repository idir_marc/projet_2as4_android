package com.iuto.qrsaver.database;

import androidx.annotation.NonNull;
import androidx.compose.ui.text.AnnotatedString;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.time.Instant;
import java.util.Date;


@Entity(tableName = "QrModel")
public class QrModel {
    @PrimaryKey(autoGenerate = true)
    private Integer id;

    private String name = null;

    @NonNull
    private String value;
    private Long date;



    public QrModel(long date, String value){
        this.value = value;
        this.date = date;
    }

    public QrModel(){
        date = Date.from(Instant.now()).getTime();
    }

    public QrModel(String s) {
        date = Date.from(Instant.now()).getTime();
        value = s;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Long getDate() {
        return date;
    }

    public void setDate(Long date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
}
