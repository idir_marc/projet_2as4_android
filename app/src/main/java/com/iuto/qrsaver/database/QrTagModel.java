package com.iuto.qrsaver.database;


import androidx.room.Entity;
import androidx.room.ForeignKey;

@Entity(tableName = "QrThModel", primaryKeys = {"id_tag","id_Qr"},
        foreignKeys = {
                @ForeignKey(entity = QrModel.class, parentColumns = "id", childColumns = "id_Qr"),
                @ForeignKey(entity = TagModel.class, parentColumns = "id", childColumns = "id_tag")})
public class QrTagModel {


    private int id_tag;
    private int id_Qr;

    public QrTagModel(int id_tag, int id){
        this.id_tag = id_tag;
        this.id_Qr = id;
    }

    public QrTagModel(){
    }

    public int getId_tag() {
        return id_tag;
    }

    public void setId_tag(int id_tag) {
        this.id_tag = id_tag;
    }

    public int getId_Qr() {
        return id_Qr;
    }

    public void setId_Qr(int id_Qr) {
        this.id_Qr = id_Qr;
    }
}
