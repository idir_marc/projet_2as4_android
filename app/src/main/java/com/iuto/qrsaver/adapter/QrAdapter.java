package com.iuto.qrsaver.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import android.content.ClipboardManager;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.LiveData;
import androidx.recyclerview.widget.RecyclerView;
import com.iuto.qrsaver.MainActivity;
import com.iuto.qrsaver.R;
import com.iuto.qrsaver.database.QrModel;
import com.iuto.qrsaver.database.RoomDAO;
import com.iuto.qrsaver.database.ThemeModel;
import com.iuto.qrsaver.ui.home.Popup_edit;
import com.iuto.qrsaver.ui.home.Popup_supprimer;
import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class QrAdapter extends RecyclerView.Adapter<QrAdapter.ViewHolder> {
    private final int layout_id;
    private List<QrModel> listeQr;
    private final Activity mainActivity;
    private Integer currentActivatedId = null;
    private List<QrAdapter.ViewHolder> allHolder = new ArrayList<>();
    private String couleur1 = "#ffffff", couleur2="#ffffff";
    private RoomDAO roomDAO;


    public QrAdapter(List<QrModel> listeQr, int layout_id, Activity context) {
        this.layout_id = layout_id;
        this.listeQr = listeQr;
        this.mainActivity = context;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        public TextView tv_texte = super.itemView.findViewById(R.id.tv_item_texte);
        public TextView tv_date = super.itemView.findViewById(R.id.tv_item_date);
        public ConstraintLayout depliableLayout = super.itemView.findViewById(R.id.depliableLayout);
        public ConstraintLayout mainLayout = super.itemView.findViewById(R.id.constraintLayoutBanal);


        public CardView cv_afficher = super.itemView.findViewById(R.id.cardView5);
        public CardView cv_edit = super.itemView.findViewById(R.id.CV_edit_nom);
        public CardView cv_copier = super.itemView.findViewById(R.id.CV_copier);
        public CardView cv_partager = super.itemView.findViewById(R.id.CV_partager);
        public CardView cv_supprimer = super.itemView.findViewById(R.id.CV_supprimer);
        public View cercle1 = super.itemView.findViewById(R.id.cercle_1);
        public View cercle2 = super.itemView.findViewById(R.id.cercle_2);


        public ViewHolder(View view){
            super(view);
        }
    }



    @NonNull
    @NotNull
    @Override
    public QrAdapter.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(this.layout_id, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint("SimpleDateFormat")
    @Override
    public void onBindViewHolder(@NonNull @NotNull QrAdapter.ViewHolder holder, int position) {
        allHolder.add(holder);
        couleur1 = "#ffffff";
        couleur2="#ffffff";

        final QrModel currentQr = this.listeQr.get(position);

        if (!(holder.tv_date == null)) {
            if (currentQr.getDate() != null) {
                Date current_date = Date.from(Instant.ofEpochMilli(currentQr.getDate()));
                SimpleDateFormat format = new SimpleDateFormat(String.format("%s 'at' %s",DateFormat.Europpean_d, DateFormat.Time_24_m));
                holder.tv_date.setText(format.format(current_date));
            }
        }
        if (!(holder.tv_texte == null)) {
            holder.tv_texte.setText(currentQr.getName());
        }

        roomDAO = MainActivity.roomDatabase.roomDAO();
        //recuperer les couleur des themes
        new Thread(
                ()->{
                    List<Integer> idThemes = roomDAO.getIdThemeAvecIdQR(currentQr.getId());
                    if (idThemes.size() > 0){
                        couleur1 = roomDAO.getEnregistrementCouleur(idThemes.get(0));
                        couleur1 = TranslateColor.translate(couleur1);
                    }

                    if (idThemes.size() > 1){
                        couleur2 = roomDAO.getEnregistrementCouleur(idThemes.get(1));
                        couleur2 = TranslateColor.translate(couleur2);
                    }



                    if (!(holder.cercle2 == null)) {
                        holder.cercle2.getBackground().setColorFilter(Color.parseColor(couleur2), PorterDuff.Mode.SRC_ATOP);
                    }

                    if (!(holder.cercle1 == null)) {
                        holder.cercle1.getBackground().setColorFilter(Color.parseColor(couleur1), PorterDuff.Mode.SRC_ATOP);
                    }
                }
        ).start();

        //click sur les carView
        holder.cv_edit.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new Popup_edit(v.getContext(), this, currentQr, holder.tv_texte).show();
                    }
                }
        );

        holder.cv_supprimer.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        new Popup_supprimer(v.getContext(), this, currentQr).show();
                    }}
        );

        holder.cv_copier.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(v.getContext(), "Lien copié", Toast.LENGTH_LONG).show();
                        ClipboardManager clipboard = (ClipboardManager) v.getContext().getSystemService(Context.CLIPBOARD_SERVICE);
                        ClipData clip = ClipData.newPlainText("test" ,currentQr.getTexte());
                        clipboard.setPrimaryClip(clip);

                    }}
        );

        holder.cv_afficher.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent( Intent.ACTION_VIEW, Uri.parse( currentQr.getTexte()) );
                        mainActivity.startActivity(intent);

                    }}
        );



        //click pour deplier
        holder.mainLayout.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //déplier la vue
                        if (holder.depliableLayout.getVisibility() == View.GONE){
                            currentActivatedId = holder.getAdapterPosition();
                            holder.depliableLayout.setVisibility(View.VISIBLE);
                            holder.mainLayout.setBackgroundColor(Color.parseColor("#bbbbbb"));
                            if (currentActivatedId != null){
                                updateCurrentActivated();
                            }
                        } else {
                            holder.depliableLayout.setVisibility(View.GONE);
                            currentActivatedId = null;
                            holder.mainLayout.setBackgroundColor(Color.WHITE);
                        }

                    }}
        );
    }


    private void updateCurrentActivated() {
        for (QrAdapter.ViewHolder h : allHolder){
            if (h.getAdapterPosition() != currentActivatedId){
                h.depliableLayout.setVisibility(View.GONE);
                h.mainLayout.setBackgroundColor(Color.WHITE);
            }
        }
    }


    @Override
    public int getItemCount() {
        return listeQr == null ? 0 : listeQr.size();
    }

    public void setListeQr(List<QrModel> l){
        this.listeQr = l;
    }

    public static abstract class DateFormat {
        public static final String Europpean_d = "dd/M/yyyy";
        public static final String American_d = "M/dd/yyyy";

        public static final String Time_24_m = "k:m";
        public static final String Time_12_m = "h:m a";
    }

    public static abstract class TranslateColor {
        public static String translate(String couleur){
            if (couleur.equals("rouge")){
                return "#f00020";
            } else if (couleur.equals("orange")){
                return "#ffa520";
            } else if (couleur.equals("bleu")){
                return "#0080ff";
            }
        return "#ffffff";
        }
    }

}
